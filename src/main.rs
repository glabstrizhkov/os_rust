#![no_std]
#![no_main]
#![feature(abi_x86_interrupt)]
#![feature(const_mut_refs)]

extern crate alloc;

use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use x86_64::VirtAddr;

mod kernel;
mod lib_rs;
mod vm;

use crate::kernel::{allocator};
use crate::kernel::task::{executor::Executor, keyboard, Task};
use kernel::kdrivers::fs::{self, BootInfoFrameAllocator};

entry_point!(kernel_main);
fn kernel_main(boot_info: &'static BootInfo) -> ! {
    kernel::init();

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { fs::init(phys_mem_offset) };
    let mut frame_allocator = unsafe { BootInfoFrameAllocator::init(&boot_info.memory_map) };

    allocator::heap::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");
    let mut executor = Executor::new();


    executor.spawn(Task::new(example_task()));
    executor.spawn(Task::new(keyboard::print_keypresses()));
    executor.run();
}

async fn async_number() -> u32 {
    42
}

async fn example_task() {
    let number = async_number().await;
    println!("async number: {}", number);
}


#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}