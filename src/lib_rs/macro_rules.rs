/// Like the `print!` macro in the standard library, but prints to the VGA text buffer.

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::kernel::kdrivers::vga_buffer::_print(format_args!($($arg)*)));
}

/// Like the `println!` macro in the standard library, but prints to the VGA text buffer.
#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

