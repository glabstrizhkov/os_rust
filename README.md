# os_rust

A pet operating system written in rust. I'm thanks full to Philipp Oppermann for the [Writing an OS in Rust blog](https://os.phil-opp.com/).
This project based on and includes most of [post-12](https://github.com/phil-opp/blog_os/tree/post-12) branch which was modified and continued by me.
More in the License section.

## Concepts & goals
- 2 level driverships. This mean that we have several low level drives, which are covering all hardware interfaces in default motherboard on first level. And decoding maps / programs on the second.
- Minimize problems of endless superstructures.
- Create a virtual machine, which will provide memory safety and single standard src -> bytecode similar to WASM to make possible using other languages.
- Max optimized to os custom language compilable to bytecode for vm.

Idea struct:
```
|-------------------|
|        VM         |
|-------------------|
| lib_rs  | kdriver |
|-------------------|
|      kernel       |
|-------------------|
|     hardware      |
|-------------------|
```

So to summarize goal of the project is developed an opensource complete system which will provide developing and using interface in high level style, but with minimal number of real abstraction.


## Building

This project requires a nightly version of Rust because it uses some unstable features. At least nightly _2020-07-15_ is required for building. You might need to run `rustup update nightly --force` to update to the latest nightly even if some components such as `rustfmt` are missing it.

You can build the project by running:

```
cargo build
```

To create a bootable disk image from the compiled kernel, you need to install the [`bootimage`] tool:

[`bootimage`]: https://github.com/rust-osdev/bootimage

```
cargo install bootimage
```

After installing, you can create the bootable disk image by running:

```
cargo bootimage
```

This creates a bootable disk image in the `target/x86_64/debug` directory.

Please file an issue if you have any problems.

## Running

You can run the disk image in [QEMU] through:

[QEMU]: https://www.qemu.org/

```
cargo run
```

[QEMU] and the [`bootimage`] tool need to be installed for this.

You can also write the image to an USB stick for booting it on a real machine. On Linux, the command for this is:

```
dd if=target/x86_64-blog_os/debug/bootimage-os_rust.bin of=/dev/sdX && sync
```

Where `sdX` is the device name of your USB stick. **Be careful** to choose the correct device name, because everything on that device is overwritten.



## License

This project is distributed under the GNU GENERAL PUBLIC LICENSE Version 3. This does not apply to post-12 code which is using as a base of this project. The post-12 is licensed under either:
- Apache License, Version 2.0 
- MIT License

which are included to the repository, but not but is not directly related to the project.
You can find more out about **Writing an OS in Rust** by Philipp Oppermann here https://os.phil-opp.com/


If you notice any violations, please let me know by email: glabstrizhkov@gmail.com